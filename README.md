# Audio amplifier

![Front image](doc/Amp-front.jpg)
![Back image](doc/Amp-back.jpg)

A simple portable audio amplifier based on PAM8403
and powered with two AA batteries.

With an additional speaker and a small component
change it can be converted to a stereo amplifier.
